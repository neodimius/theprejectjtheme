<?php 
    class ThemeSettings{
        public function __construct(){
            add_action ('wp_enqueue_scripts', array($this, 'addScripts'));
            add_action('init', array($this, 'sliderPost'));
            add_action('init', array($this, 'jewelryItems'));
            add_action('after_setup_theme', array($this, 'add_posts_formats'));
            add_action( 'widgets_init', array($this, 'register_my_widgets'));
            add_action('init', array($this, 'create_jewelryTaxonomy'));
        }

        public function addScripts(){
            wp_enqueue_script( 'jquery', get_template_directory_uri() . '/assets/js/jquery-3.3.1.min.js', '', '', true);
            wp_enqueue_script( 'popper', get_template_directory_uri() . '/assets/js/popper.min.js', '', '', true);
            wp_enqueue_script( 'bootstrapJS', get_template_directory_uri() . '/assets/js/bootstrap.min.js', '', '', true);

            wp_enqueue_style('style', get_stylesheet_uri());
            wp_enqueue_style( 'bootstrapcss', get_template_directory_uri() . '/assets/css/bootstrap.min.css');
        }

        public function add_posts_formats (){
            add_theme_support('post-thumbnails'); // Plug Thumbnails (Future image)
            add_theme_support( 'custom-background' );
            add_theme_support( 'post-formats', array('aside', 'galery', 'image') );
            add_theme_support( 'custom-header' );
            add_theme_support( 'custom-logo' );
            
        }

        function register_my_widgets(){
            register_sidebar( array(
            'name' => "Правая боковая панель сайта",
            'id' => 'right-sidebar',
            'description' => 'Эти виджеты будут показаны с правой колонке сайта',
            'before_title' => '<h1>',
            'before_widget' => '<div>',
            'after_widget' => '</div>',
            'after_title' => '</h1>'
            ) );
    
            register_sidebar( array(
                'name' => "Left боковая панель сайта",
                'id' => 'left-sidebar',
                'description' => 'Эти виджеты будут показаны с правой колонке сайта',
                'before_title' => '<h1>',
                'before_widget' => '<div>',
            'after_widget' => '</div>',
                'after_title' => '</h1>'
                ) );
     
    
            $args = array(
                'name' => "Footer %d",
                'id' => 'footer',
                'description' => 'Эти виджеты будут показаны с правой колонке сайта',
                'before_title' => '<h1>',
                'before_widget' => '<div>',
                'after_widget' => '</div>',
                'after_title' => '</h1>'
            );
            register_sidebars(4, $args);
                
        }

        public function sliderPost(){
            $lables = array(
            'name' => 'Slider',
            'singular_name' => 'Slider',
            'add_new' => 'Add Item',
            'all_items' => 'All items',
            'add_new_item' => 'Add item',
            'edit_item' => 'Edit item',
            'new_item' => 'New item',
            'view_item' => 'View item',
            'search_items' => 'Search',
            'not_found' => 'No item found',
            'not_found_in_trash' => 'No item found in trash',
            'parent_item_colon' => 'Parent item',
            'menu_name' => 'HeaderSlider'
            );
            
            $args = array(
            'labels' => $lables,
            'public' => true,
            'has_archive' => 'slider',
            'publicly_queryable' => true,
            'query_var' => true,
            'rewrite' => true,
            'capability_type' => 'post',
            'hierarchical' => false,
            'supports' => array('title','editor','thumbnail','excerpt','trackbacks','custom-fields','comments','revisions','page-attributes','post-formats'),
            'taxonomies' => array('category', 'post_tag'),
            'menu_position' => 2,
            'exclude_from_search' => false
        );
            register_post_type('slider', $args);
        }

        public function jewelryItems(){
            $lables = array(
            'name' => 'jewelryItems',
            'singular_name' => 'jewelryItems',
            'add_new' => 'Add Item',
            'all_items' => 'All items',
            'add_new_item' => 'Add item',
            'edit_item' => 'Edit item',
            'new_item' => 'New item',
            'view_item' => 'View item',
            'search_items' => 'Search',
            'not_found' => 'No item found',
            'not_found_in_trash' => 'No item found in trash',
            'parent_item_colon' => 'Parent item',
            'menu_name' => 'Jewelry Items'
            );
            
            $args = array(
            'labels' => $lables,
            'public' => true,
            'has_archive' => 'jewelryItems',
            'publicly_queryable' => true,
            'query_var' => true,
            'rewrite' => true,
            'capability_type' => 'post',
            'hierarchical' => false,
            'supports' => array('title','editor','thumbnail','excerpt','comments','revisions','page-attributes','post-formats'),
            'taxonomies' => array('category', 'genres'),
            'menu_position' => 2,
            'exclude_from_search' => false
        );
            register_post_type('jewelryItems', $args);
        }
    
    public function create_jewelryTaxonomy(){
        $labels = array(
        'name' => 'typeJewelry',
        'singular_name' => 'typeJewelry',
        'search_items' => 'Search Jewelry',
        'all_items' => 'All Jewelry',
        'parent_item' => 'Parent Jewelry',
        'parent_item_colon' => 'Parent Jewelry:',
        'edit_item' => 'Edit Jewelry',
        'update_item' => 'Update Jewelry',
        'add_new_item' => 'Add New Jewelry',
        'new_item_name' => 'New Jewelry Name',
        'menu_name' => 'JewelryType',
        );
        
        $args = array(
        'label' => '',
        'labels' => $labels,
        'description'  => '',
        'public' => true,
        'publicly_queryable' => null,
        'show_in_nav_menus' => true,
        'show_ui' => true,
        'show_tagcloud' => true,
        'show_in_rest' => null,
        'rest_base' => null,
        'hierarchical' => true,
        'update_count_callback' => '',
        'rewrite' => true,
        'capabilities' => array(),
        'meta_box_cb' => null,
        'show_admin_column' => false,
        '_builtin' => false,
        'show_in_quick_edit' => null,
        );
        register_taxonomy('typeJewelry', 'jewelryItems', $args );
        }
    }

    

    new ThemeSettings;
?>