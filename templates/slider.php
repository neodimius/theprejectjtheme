
<?php
    $query = new WP_Query(array('post_type' => 'slider'));
    $postsCount =count ($query -> posts);
    $count = 0;
?>

  <!--    Carousel -->

<div id="myCarousel" class="carousel slide p-0" data-ride="carousel" data-interval="3000">
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <?php for ($i = 1; $i < $postsCount; $i++):?>
                <li data-target="#myCarousel" data-slide-to="<?php echo $i;?>"></li>
            <?php endfor;?>
        </ol>
        <div class="carousel-inner">
        
        <?php while ($query -> have_posts()) : $query->the_post();?>
            <?php $count++; ?>
            <?php if ($count == 1) : ?>
            <div class="carousel-item active">
                <img class="first-slide" src="<?php echo get_the_post_thumbnail_url();?>" alt="First slide">
                <div class="container">
                    <div class="carousel-caption text-left">
                        <h1 class = "bg-secondary"><?php echo get_the_title();?></h1>
                        <p><?php echo get_the_content();?></p>
                    </div>
                </div>
            </div>

            <?php else : ?>
                <?php $count = 2 ? $imgClass = "second-slide" : $imgClass = "third-slide";?>
            <div class="carousel-item">
                <img class="<?php echo $imgClass;?>" src="<?php echo get_the_post_thumbnail_url();?>" alt="<?php echo $imgClass;?>">
                <div class="container">
                    <div class="carousel-caption text-left">
                        <h1 class = "bg-secondary"><?php the_title();?></h1>
                        <p><?php echo get_the_content();?></p>
                    </div>
                </div>
            </div>
            <?php endif; ?>
            <?php endwhile; ?>

          
        
        <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
</div>

