<div class="container">
    <div class="row justify-content-center">
        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
            <a href="<?php the_permalink();?>">
                <div class="col-md-6" style="background: silver;">
                    <?php
                        the_title('<h3>', '</h3>');
                        the_excerpt();                // SHort Description
                    ?>
                    <div>
                        <img src="<?php echo get_the_post_thumbnail_url();?>" class="img-fluid"> 
                    </div>
                </div>
        </a>
            <?php endwhile; else : ?>
            <h2><?php _e( 'Вибачте, але за вашим запитом нічого не знайдено :( ' ); ?></h2>
        <?php endif; ?>
    </div>
</div>