<?php $category = new WP_Query(array('post_type' => 'jewelryItems'));?>

<div class="container-fluid">
    <h1 class="bg-secondary">Наші ексклюзивні роботи:</h1>
    <div class="row d-flex justify-content-between">
<?php $query = new WP_Query(array('post_type' => 'jewelryItems'));?>

<?php while ($query -> have_posts()) : $query->the_post();?>
        <div class="col-md-3">       
            <div class="card" style="width: 18rem;">
            <img class="card-img-top" src="<?php echo get_the_post_thumbnail_url();?>" alt="Card image cap">
            <div class="card-body">
                <h5 class="card-title"><?php the_title();?></h5>
                <p class="card-text"><?php the_excerpt();?></p>
                <a href="<?php the_permalink();?>" class="btn btn-primary">More info</a>
            </div>
            </div>
        </div>
<?php endwhile;?>    
    </div>
</div>