<div class="container">
	<div class="row">
	<?php $query = new WP_Query(array('post_type' => 'post'));?>
	<?php while ($query -> have_posts()) : $query->the_post();?>
		<div class="col-md-4">
			<div class="row bg-secondary"><h2><?php the_title();?></h2></div>
			<div class="row"><img src="<?php echo get_the_post_thumbnail_url();?>" alt=""></div>
			<div class="row"><?php the_excerpt();?></div>
		</div>
		<?php endwhile;?>
	</div>

</div>	