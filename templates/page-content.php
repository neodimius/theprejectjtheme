<div class="container">
    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    
    <?php
        the_title('<h1>', '</h1>');
        the_content();
        
        
        the_post_thumbnail();
    ?>
  
    <?php endwhile; else : ?>
    <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
    <?php endif; ?>
</div>