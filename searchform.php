<form role="search" method="get" id="searchform" action="<?php echo home_url( '/' ) ?>" class="form-inline my-2 my-lg-0">
    <input class="form-control mr-sm-2" type="text" value="<?php echo get_search_query() ?>" name="s" id="s" placeholder="Введіть запит" aria-label="Search"/>
    <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Пошук</button>
</form>
