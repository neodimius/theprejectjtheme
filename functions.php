<?php
    include "classes/ThemeSettings.php";
    add_action('init', 'create_taxonomy');
        function create_taxonomy(){
        $labels = array(
        'name' => 'Genres',
        'singular_name' => 'Genres',
        'search_items' => 'Search Genres',
        'all_items' => 'All Genres',
        'parent_item' => 'Parent Genre',
        'parent_item_colon' => 'Parent Genre:',
        'edit_item' => 'Edit Genre',
        'update_item' => 'Update Genre',
        'add_new_item' => 'Add New Genre',
        'new_item_name' => 'New Genre Name',
        'menu_name' => 'Genre',
        );
        
        $args = array(
        'label' => '',
        'labels' => $labels,
        'description'  => '',
        'public' => true,
        'publicly_queryable' => null,
        'show_in_nav_menus' => true,
        'show_ui' => true,
        'show_tagcloud' => true,
        'show_in_rest' => null,
        'rest_base' => null,
        'hierarchical' => true,
        'update_count_callback' => '',
        'rewrite' => true,
        'capabilities' => array(),
        'meta_box_cb' => null,
        'show_admin_column' => false,
        '_builtin' => false,
        'show_in_quick_edit' => null,
        );
        register_taxonomy('genres', 'jewelryItems', $args );
        }
?>

