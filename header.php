<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <?php wp_head();?>
</head>
<body>

<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
      <a class="navbar-brand" href="#"><?php echo get_custom_logo(); ?></a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
          <?php foreach ( wp_get_nav_menu_items('main') as $key => $menu_item ):?>
          <li class="nav-item active">
            <a class="nav-link" href="<?php echo  $menu_item->url; ?>"><?php echo  $menu_item->title; ?></a>
          </li>
          <?php endforeach;?>
        </ul>
        <?php get_search_form()?>
      </div>
	</nav>
  
  


  